<?php  include_once('../Controller/DemandeController.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Effectuer une demande de congé</title>
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript"> 
$('#checky').click(function(){
     
     if($conge_legal(this).attr('checked') == false){
          $('#Document_conf').attr("disabled","disabled");   
     }
     else {
         $('#Document_conf').removeAttr('disabled');
     }
 });
  
        } 
    </script>
</head>
<body>
  <div class="header">
  	<h2>Formulaire de demande de congé</h2>
  </div>
<!------ Include the above in your HEAD tag ---------->

<form class="form-horizontal" action='#' method="POST">
  <fieldset>
    <div class="control-group">
      <!-- Username -->
      <label class="control-label"  for="Date_Debut">Date de debut</label>
      <div class="controls">
        <input type=date data-provide="datepicker" id="Date_Debut" name="Date_Debut" placeholder="">
      </div>
    </div>
 
    <div class="control-group">
      <!-- E-mail -->
      <label class="control-label" for="Duree">Durée du congé</label>
      <div class="controls">
        <input type="text" id="Duree" name="Duree" placeholder="30" class="input-large">
        <p class="help-block">Veiller saisir une durée en jours</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="Type">Conge legal?</label>
      <div class="controls">
        <input type="checkbox" checked="checked" id="conge_legal" name="conge_legal" class="input-large">
        <p class="help-block">Cocher s'il s'agit d'un conge legla</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password -->
      <label class="control-label"  for="password_confirm">Document de conforminté </label>
      <div class="controls">
        <input type="file" id="Document_conf" name="Document_conf" class="input-large">
        <p class="help-block">Ajouter le fichier</p>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Button -->
      <div class="controls">
        <input type="submit" class="btn btn-success">
      </div>
    </div>
  </fieldset>
</form>
</body>
</html>