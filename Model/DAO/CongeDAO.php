<?php
require_once("/var/www/html/conge/Model/InterfaceMetier/Conge_Interface.php");
class CongeDAO implements Conge_interface
{
    private $bd;

    // le constructeur

    public function __construct($bdVarLocal)
    {
        $this->setBd($bdVarLocal);
    }

    /**
     * fonction retournant la connexion à la base de donnée
     **/

    public function setBd(PDO $bdVarLocal)
    {
        $this->bd = $bdVarLocal;
    }

    public function Ajouter_Conger($type_conge, $Date_Debut, $Duree, $MatriculeColla, $LienDoc)
    {
        $requete = $this->bd->prepare('INSERT INTO Conge(idConge, type_conge, MatriCollab, DateDebut, Duree, LienDoc) VALUES(DEFAULT, :type_conge , :MatriCollab, :DateDebut, :Duree, :LienDoc');
        try 
        {
            $requete->execute(array('type_conge'=>$type_conge, 'DateDebut'=>$DateDebut, 'Duree'=>$Duree, 'LienDoc'=>$LienDoc, 'MatriCollab'=>$MatriculeColla ));
        } catch (Exception $e) 
        {
            die('Erreur : '.$e->getMessage());
        }

    }
}
?>